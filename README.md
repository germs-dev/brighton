---
title: Regency Mews holiday let
author: info@mews.one
---

## Contact
- info@mews.one
- Instagram: [@mews.one](https://www.instagram.com/mews.one/)

## Bars and cocktails

- [Gung Ho!](https://www.gunghobar.com/) -- tiny but great cocktail bar just up the road from the Mews
- [The Plotting Parlour](https://theplottingparlour-brighton.co.uk/) -- gorgeous cocktail lounge, be sure to book!
- [Artist Residence](https://www.artistresidence.co.uk/brighton/eat-drink/) -- down the road and left along Regency Square, great cocktails in a cosy bar
- [Browns](https://www.browns-restaurants.co.uk/restaurants/southeast/brighton#/) -- further into town
- [Rockwater](https://rockwater.uk/) -- along Hove beach side, stylish bar/restaurant on two levels

## Food
### Regency
- [Bacqueano](https://www.baqueano.co.uk/) -- wonderful Argentinian steakhouse!
- [Cyan at The Grand](https://cyanbrighton.co.uk/) -- lovely restaurant in a great setting
- [Naninella](https://www.nanninellapizzeria.co.uk/) -- great pizza!
- [Goemon](https://goemonramen.com/) -- for good value, chilled Japanese cafe style, at the bottom of Preston Street
- [Bincho Yakitori](https://www.binchoyakitori.com/) -- top of Preston Street, a cool and delicious Japanese yakitori place
- [The New Club](https://thenewclubbrighton.com/) -- at the bottom of the Preston Street. nice for breakfast and brunch/lunch

### Beach
- [The Flour Pot](https://www.theflourpot.co.uk/locations/brighton-beach/) -- a lovely, local bakery and cafe chain on the front, near the i360: great baked goods, lunch and coffee
- [Riddle and Finn's](https://www.riddleandfinns.co.uk/) -- champagne and oyster bars
- [Due South](https://www.duesouthrestaurant.co.uk/) and [Murmur](https://murmur-restaurant.co.uk/)
- [Shelter Hall](https://shelterhall.co.uk/)
- [The Salt Room](https://saltroom-restaurant.co.uk/) -- spacious, glamorous sea-facing restaurant, specialising in seafood

<!--
- [Soho House](https://www.sohohouse.com/houses/brighton-beach-house) (members only!) -- Brighton 
-->

### Central
- [Furna](https://furnarestaurant.co.uk/) -- amazing new kid on the block
- [Burnt Orange](https://burnt-orange.co.uk/) -- lovely, stylish, relaxed restaurant... gorgeous!
- [The Coalshed](https://coalshed-restaurant.co.uk/) -- excellent steak restaurant from the same owner as The Saltroom and Burnt Orange
- [64 Degrees](https://64degrees.co.uk/) -- tasting menu restaurant, tiny place, very good!
- [The Ivy in The Lanes](https://theivybrighton.com/)
- [The Ivy Asia](https://theivyasiabrighton.com/)
- [Chilli Pickle](https://thechillipickle.com/)
- [The Flint House](https://www.flinthousebrighton.com/)
- [Manju's](https://manjus.co.uk/)
- [The Set](https://www.thesetrestaurant.com/) -- cool venue for a tasting menu
- [Petit Pois](https://petitpoisbrighton.co.uk/)
- [Issac At](https://www.isaac-at.com/)
- [Wild Flor](https://www.wildflor.com/)
- [Moshimo](https://www.moshimo.co.uk/)

### Hove
- [etch.](https://www.etchfood.co.uk/) -- delicious tasting menu
- [The Little Fish Market](https://www.thelittlefishmarket.co.uk/) -- you really do have to book here as it's tiny but probably the best ﬁsh restaurant in Brighton!
- [Fourth and Church](https://www.fourthandchurch.co.uk/)
- [Purezza](https://purezza.co.uk/)

### Kemptown
- [Vel](https://velbrighton.co.uk/) -- amazingly spiced south Indian food
- [Lucky Khao](https://www.luckykhao.com/) is great. Modern funky Thai!
- [Block Bar](https://blockbar.co.uk/)

### Vegetarian/vegan
- [Bonsai Plant Kitchen](https://www.bonsaiplantkitchen.co)
- [No Catch](https://www.nocatch.co/)
- [Terre à Terre](https://terreaterre.co.uk/) -- long-running vegetarian restaurant
- [Food For Friends](https://www.foodforfriends.com/)
- [Botanique](https://www.botaniquebrighton.com/)
- [What The Pitta](https://whatthepitta.com/what-the-pitta-brighton-2/)

### Burgers
- 7 Bone
- Burger Brothers
- Meat Liquor
- Wendy's
- Smash

## Places to visit
- [i360](https://brightoni360.co.uk/tickets/) -- at the bottom of Preston Street
- [Royal Pavilion & Garden](https://brightonmuseums.org.uk/visit/royal-pavilion-garden/)
- [Sealife Centre](https://www.visitsealife.com/brighton/)
- [Volk's Electric Railway](https://volksrailway.org.uk/) -- reopens Easter 2023
- [Brighton Marina](https://www.brightonmarina.co.uk/)
- [The Undercliff Walk](https://www.brighton-hove.gov.uk/libraries-leisure-and-arts/seafront/undercliff-walk)
- [Palace Pier](https://www.brightonpier.co.uk/)
- [Brighton Museum & Art Gallery](https://brightonmuseums.org.uk/brighton-museum-art-gallery/)
- [Theatre Royal](http://theatreroyalbrighton.com/) -- catch a show
- [Komedia](https://www.komedia.co.uk/brighton/)
- [The Lanes](https://www.visitbrighton.com/food-and-drink/areas-to-explore/the-lanes) -- shopping and a bite to eat

### For kids
- [Brighton zip line](https://brightonzip.com/)
- [Upside down house](https://upsidedownhouse.co.uk/)
- [Laserzone](https://www.laserzone.co.uk/brighton/)

### Outside of town
- [Devil's Dyke](https://www.nationaltrust.org.uk/visit/sussex/devils-dyke) -- north of Brigton, spectacular views across The Downs, great for a walk and a pub at the end
- [The Chattri Memorial](https://www.brighton-hove.gov.uk/libraries-leisure-and-arts/parks-and-green-spaces/chattri-memorial) -- worth the walk up The Downs for this serene and beautiful memorial 

## Live music
- [Brighton Dome](https://brightondome.org/)
- [Chalk](https://chalkvenue.com/)
- [Green Door Store](https://thegreendoorstore.co.uk/)
- [Concorde 2](https://www.concorde2.co.uk/)

